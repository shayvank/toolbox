﻿$user = $args[0]
if ($user -eq $null) {
    throw "No username defined"
}


(New-Object System.DirectoryServices.DirectorySearcher("(&(objectCategory=User)(samAccountName=$($user)))")).FindOne().GetDirectoryEntry().memberOf | % { (New-Object System.DirectoryServices.DirectoryEntry("LDAP://"+$_)) } | Sort-Object sAMAccountName | SELECT @{name="Group Name";expression={$_.Name}} 