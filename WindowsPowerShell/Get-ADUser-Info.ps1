﻿$user = $args[0]
if ($user -eq $null) {
    throw "No username defined"
}

$user = (New-Object System.DirectoryServices.DirectorySearcher("(&(objectCategory=User)(samAccountName=$($user)))")).FindOne()

if ($user -ne $null) {
    $direntry = $user.GetDirectoryEntry()
    Write-host "Name: " $direntry.Name
    $props = $direntry.Properties
    $propnames = $props.PropertyNames
    foreach ($name in $propnames) {
        if ($name -in "displayName", "telephoneNumber", "distinguishedName","whenChanged", "displayName", "sAMAccountName", "userPrincipalName") { 
            Write-host $name":" $props.item($name)
        } elseif ($name -in "lastLogon","pwdLastSet","accountExpires","lockoutTime","lastLogonTimestamp") {
            $valueint = $direntry.ConvertLargeIntegerToInt64($props.item($name).value)
            if ($valueint -eq 0) {
                Write-host $name": none"
            } else {
                $datevalue = [DateTime]::FromFileTime($valueint)
                Write-host $name":" $datevalue
            }
        }
    }
}